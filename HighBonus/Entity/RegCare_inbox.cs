//------------------------------------------------------------------------------
// <auto-generated>
//     這個程式碼是由範本產生。
//
//     對這個檔案進行手動變更可能導致您的應用程式產生未預期的行為。
//     如果重新產生程式碼，將會覆寫對這個檔案的手動變更。
// </auto-generated>
//------------------------------------------------------------------------------

namespace HighBonus.Entity
{
    using System;
    using System.Collections.Generic;
    
    public partial class RegCare_inbox
    {
        public string Id { get; set; }
        public string DXID { get; set; }
        public Nullable<int> Status { get; set; }
        public Nullable<int> SendCount { get; set; }
        public string ISUSERM { get; set; }
        public string ISIOS { get; set; }
        public string VIN { get; set; }
        public string Title { get; set; }
        public string Name { get; set; }
        public string DLRNM { get; set; }
        public string BRNHSRV { get; set; }
        public string BRTEL { get; set; }
        public string LICSNO_F { get; set; }
        public string USEFRE { get; set; }
        public string FXDTYY { get; set; }
        public string FXDES { get; set; }
        public Nullable<int> POINT { get; set; }
        public string NDMMSG { get; set; }
        public string DMMSG { get; set; }
        public string MEMO { get; set; }
        public Nullable<System.DateTime> PROCDT { get; set; }
        public System.DateTime CreateDate { get; set; }
        public string EXPIRE { get; set; }
        public string PART { get; set; }
        public string CHANGEDSC { get; set; }
        public Nullable<int> ISFM { get; set; }
        public string ISFROIL { get; set; }
        public string VALIDDT { get; set; }
        public string Exchange { get; set; }
        public string PARTNO1 { get; set; }
        public string PARTNM1 { get; set; }
        public Nullable<int> POINT1 { get; set; }
        public string IMG1 { get; set; }
        public string PARTNO2 { get; set; }
        public string PARTNM2 { get; set; }
        public Nullable<int> POINT2 { get; set; }
        public string IMG2 { get; set; }
        public string PARTNO3 { get; set; }
        public string PARTNM3 { get; set; }
        public Nullable<int> POINT3 { get; set; }
        public string IMG3 { get; set; }
        public Nullable<System.DateTime> JLCDT { get; set; }
        public Nullable<double> JMV { get; set; }
        public Nullable<System.DateTime> KLCDT { get; set; }
        public Nullable<double> KMV { get; set; }
        public Nullable<System.DateTime> LLCDT { get; set; }
        public Nullable<double> LMV { get; set; }
        public Nullable<System.DateTime> ILCDT { get; set; }
        public Nullable<double> IMV { get; set; }
        public Nullable<System.DateTime> CLCDT { get; set; }
        public Nullable<System.DateTime> DLCDT { get; set; }
        public string ContentIMG { get; set; }
        public int EXTRAAMT1 { get; set; }
        public int EXTRAAMT2 { get; set; }
        public int EXTRAAMT3 { get; set; }
        public Nullable<System.DateTime> MCLDT { get; set; }
    }
}
