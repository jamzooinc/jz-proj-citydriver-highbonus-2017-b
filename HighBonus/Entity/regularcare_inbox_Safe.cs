//------------------------------------------------------------------------------
// <auto-generated>
//     這個程式碼是由範本產生。
//
//     對這個檔案進行手動變更可能導致您的應用程式產生未預期的行為。
//     如果重新產生程式碼，將會覆寫對這個檔案的手動變更。
// </auto-generated>
//------------------------------------------------------------------------------

namespace HighBonus.Entity
{
    using System;
    using System.Collections.Generic;
    
    public partial class regularcare_inbox_Safe
    {
        public string Id { get; set; }
        public string DXID { get; set; }
        public Nullable<int> ISIOS { get; set; }
        public Nullable<int> Type { get; set; }
        public Nullable<int> Status { get; set; }
        public int SendCount { get; set; }
        public string TITLE { get; set; }
        public string LICSNO { get; set; }
        public string CUSTNM { get; set; }
        public Nullable<System.DateTime> USTDAT { get; set; }
        public Nullable<System.DateTime> FENDAT { get; set; }
        public Nullable<System.DateTime> UENDAT { get; set; }
        public string BRAND { get; set; }
        public string DLRCD { get; set; }
        public string BRNHCD { get; set; }
        public string SECTCD { get; set; }
        public string SALENM { get; set; }
        public string SALES_EMAIL { get; set; }
        public string SALEMG { get; set; }
        public string SALEMG_EMAIL { get; set; }
        public string TELO { get; set; }
        public Nullable<System.DateTime> CRTDT { get; set; }
        public Nullable<System.DateTime> OpenPushDate { get; set; }
        public Nullable<System.DateTime> SendDate { get; set; }
        public System.DateTime CreateDate { get; set; }
    }
}
