﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HighBonus.Models
{
    #region 利high金
    public class HighPointModel
    {
        //推播用基本欄位
        public string Id { get; set; }           //編號
        public string DXID { get; set; }         //DXID
        public int Status { get; set; }       //狀態 0:未發送 , 1:已發送  2:已讀取 , 3:已刪除 , 4:錯誤 , 5:用戶關閉
        public DateTime PROCDT { get; set; }       //發送日期 推播產生日 EX: 20140702 | DateTime
        public DateTime CreateDate { get; set; } //建立日期 | DateTime
        public string Title { get; set; }       //推播標題:TOYOTA車主權益提醒：您的利high金共有XXX點，歡迎詢問兌換

        //客製化欄位
        public string LICSNO { get; set; }       //車號  EX :**-4071
        public int POINT { get; set; }       //客戶擁有的利high金點數
        public string INVOCID { get; set; } //身份證字號
        public string PARTNO1 { get; set; } //精品代碼1
        public string PARTNM1 { get; set; } //精品名稱1
        public int POINT1 { get; set; }       //精品代碼1需要點數
        public string IMG1 { get; set; }  //精品照片網址1
        public string PARTNO2 { get; set; } //精品代碼2
        public string PARTNM2 { get; set; } //精品名稱2
        public int POINT2 { get; set; }       //精品代碼2需要點數
        public string IMG2 { get; set; }  //精品照片網址2
        public string PARTNO3 { get; set; } //精品代碼3
        public string PARTNM3 { get; set; } //精品名稱3
        public int POINT3 { get; set; }       //精品代碼3需要點數
        public string IMG3 { get; set; }  //精品照片網址3

        public string Url { get; set; } //組出來的可登入網址
        //是否已預約保養過了
        public bool IsOrderByLicsno { get; set; }
        //車牌號碼
        public string LICSNO_F { get; set; }
        public string VALIDDT { get; set; }

        //public List<DXIDS> DXIDS { get; set; }      //DXid集合

        //public string ADD_LIST_SET { get; set; }      //追加工作項目,套餐
        //public List<HighItem> HighPointItem { get; set; }      //追加工作項目,套餐

        public HighPointModel()
        {
            PROCDT = DateTime.Now;
            CreateDate = DateTime.Now;
            Title = "TOYOTA車主權益提醒：您的利high金共有XXX點，歡迎詢問兌換";
        }
    }
    //DXID列表
    //public class DXIDS
    //{
    //    public string DXID { get; set; }
    //}
    //和泰建議項目
    //public class HighItem
    //{
    //    public string Code { get; set; }
    //    public string Name { get; set; }
    //    public int Point { get; set; }
    //    public string Url { get; set; }
    //}
    #endregion
}