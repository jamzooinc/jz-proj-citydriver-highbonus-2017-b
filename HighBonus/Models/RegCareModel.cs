﻿using System;

namespace HighBonus.Models
{
    public class RegCareModel
    {
        //20150713 機油卷資料

        public string FROTITLE { get; set; }
        //是否需新增機油券
        public string ISFROIL { get; set; }
        //機油券有效日期
        public string VALIDDT { get; set; }
        //是否有兌換機油券
        public string Exchange { get; set; }
        //車牌號碼
        public string LICSNO { get; set; }

        //20150225 影音定保增加的欄位
        //1 首半年 
        //2 萬公里定保
        public int ISFM { get; set; }

        //是否已預約保養過了
        public bool IsOrderByLicsno { get; set; }

        //編號
        public string Id { get; set; }

        //裝置編號
        public string DXID { get; set; }

        //狀態 0:未發送 , 1:已發送 , 2:已讀取 , 3:已刪除 
        public int Status { get; set; }

        //已推播次數 (保留欄位)
        public int SendCount { get; set; }

        //此裝置的手機號碼是否為 使用人號碼 EX: Y :是
        public string ISUSERM { get; set; }

        //車機與手機裝置區別 EX: M 為手機 C為車機
        public string ISIOS { get; set; }

        //車身號碼 (引擎號碼)
        public string VIN { get; set; }

        //推播標題 ,由和泰一併傳入
        public string Title { get; set; }

        //車主姓名
        public string Name { get; set; }

        //經銷商名稱 EX 蘭陽豐田
        public string DLRNM { get; set; }

        //服務專員 EX 羅東服務廠 陳文龍
        public string BRNHSRV { get; set; }

        //服務專線 EX 03-9658888
        public string BRTEL { get; set; }

        //車牌號碼
        public string LICSNO_F { get; set; }

        //保養週期  EX:每半年定保
        public string USEFRE { get; set; }

        //定保日期  EX:103年
        public string FXDTYY { get; set; }

        //定保內容  EX:17年保養
        public string FXDES { get; set; }

        //更換項目 EX: 更換項目:'機油、機油濾清器、放油塞墊片
        public string CHANGEDSC { get; set; }

        //紅利點數到期日
        public string EXPIRE { get; set; }

        //新上市精品
        public string PART { get; set; }

        //紅利好康累積點數 EX:累積點數:0 點
        public int POINT { get; set; }

        //活動快訊
        public string NDMMSG { get; set; }

        //服務資訊
        public string DMMSG { get; set; }

        //貼心提醒
        public string MEMO { get; set; }

        //發送日期=推播產生日 EX: 20140702
        public DateTime PROCDT { get; set; }

        //建立日期
        public DateTime CreateDate { get; set; }

        public string Os { get; set; }    
        public int? OsNum { get; set; } //1:iOS /2:Android/3:CarMode 請傳入 1~3 代號即可
        public bool? ISSEEVIDEO { get; set; } //是否有有在推播內打開影片
        public DateTime? SEEVIDEOTime { get; set; }
        public bool? ISORDERINBOX { get; set; }  //是否有在推播內點擊預約保養按鈕
        public DateTime? ORDERINBOXTime { get; set; }
        public bool? ISSEERECORD { get; set; } //是否有在推播內點擊預約紀錄按鈕
        public DateTime? SEERECORDTime { get; set; }
        public bool? ISORDERAPP {get;set;} //是否有從定保推播點擊預約保養按鈕至App確認預約
        public DateTime? ORDERAPPTime { get; set; }
        public string ContentIMG { get; set; }
        public string PARTNO1 { get; set; } //精品代碼1
        public string PARTNM1 { get; set; } //精品名稱1
        public int? POINT1 { get; set; }       //精品代碼1需要點數
        public int EXTRAAMT1 { get; set; }
        public string IMG1 { get; set; }  //精品照片網址1
        public string PARTNO2 { get; set; } //精品代碼2
        public string PARTNM2 { get; set; } //精品名稱2
        public int? POINT2 { get; set; }       //精品代碼2需要點數
        public int EXTRAAMT2 { get; set; }
        public string IMG2 { get; set; }  //精品照片網址2
        public string PARTNO3 { get; set; } //精品代碼3
        public string PARTNM3 { get; set; } //精品名稱3
        public int? POINT3 { get; set; }       //精品代碼3需要點數
        public int EXTRAAMT3 { get; set; }
        public string IMG3 { get; set; }  //精品照片網址3
        //零件更換建議
        /*
輪胎前次更換時間	JLCDT
輪胎前次量測深度	JMV 
前煞車塊更換時間	KLCDT 
前煞車塊量測厚度	KMV
後煞車塊更換時間	LLCDT
後煞車塊量測厚度	LMV
電瓶更換時間	ILCDT
電瓶量測數值	IMV
空調濾網前次更換時間	CLCDT
燃油濾清器更換時間	DLCDT
火星塞更換時間	MCLDT
         */
        public DateTime? JLCDT { get; set; }
        public string JMV { get; set; }
        public DateTime? KLCDT { get; set; }
        public string KMV { get; set; }
        public DateTime? LLCDT { get; set; }
        public string LMV { get; set; }
        public DateTime? ILCDT { get; set; }
        public string IMV { get; set; }
        public DateTime? CLCDT { get; set; }
        public DateTime? DLCDT { get; set; }
        public DateTime? MCLDT { get; set; }

        public RegCareModel()
        {
            CreateDate = DateTime.Now;
            Exchange = string.Empty;
        }
    }
}