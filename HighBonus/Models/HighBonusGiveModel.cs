﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HighBonus.Models
{
    #region 利high金
    public class HighBonusGiveModel
    {
        //推播用基本欄位
        public string Id { get; set; }           //編號
        public string DXID { get; set; }         //DXID
        public int Status { get; set; }       //狀態 0:未發送 , 1:已發送  2:已讀取 , 3:已刪除 , 4:錯誤 , 5:用戶關閉
        public DateTime PROCDT { get; set; }       //發送日期 推播產生日 EX: 20140702 | DateTime
        public DateTime CreateDate { get; set; } //建立日期 | DateTime
        public string Title { get; set; }       //推播標題:TOYOTA車主權益提醒：您的利high金共有XXX點，歡迎詢問兌換

        //客製化欄位
        public string LICSNO { get; set; }       //車號  EX :**-4071

        //車牌號碼
        public string LICSNO_F { get; set; }
        public string VALIDDT { get; set; }
        //是否已預約保養過了
        public bool IsOrderByLicsno { get; set; }

        //public List<DXIDS> DXIDS { get; set; }      //DXid集合

        //public string ADD_LIST_SET { get; set; }      //追加工作項目,套餐
        //public List<HighItem> HighPointItem { get; set; }      //追加工作項目,套餐

        public HighBonusGiveModel()
        {
            PROCDT = DateTime.Now;
            CreateDate = DateTime.Now;
        }
    }
    //DXID列表
    //public class DXIDS
    //{
    //    public string DXID { get; set; }
    //}
    //和泰建議項目
    //public class HighItem
    //{
    //    public string Code { get; set; }
    //    public string Name { get; set; }
    //    public int Point { get; set; }
    //    public string Url { get; set; }
    //}
    #endregion
}