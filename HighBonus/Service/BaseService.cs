﻿using HighBonus.Entity;

namespace HighBonus.Service
{
    public class BaseService
    {
        public citydriverEntities basedb;

        public BaseService()
        {
            basedb = new citydriverEntities();
        }

        public static string Domain = System.Web.Configuration.WebConfigurationManager.AppSettings["Domain"];
    }
}