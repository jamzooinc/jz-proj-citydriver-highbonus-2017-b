﻿using HighBonus.Entity;
using HighBonus.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using APushProvider.Core;
using System.Configuration;
using System.Reflection;

namespace HighBonus.Service
{
    public class HighBunusInboxService : BaseService
    {
        public HighPointModel GetHighModel(string PushId, string DXID = null)
        {
            HighBonus_inbox inbox;
            inbox = !string.IsNullOrEmpty(DXID)
                ? basedb.HighBonus_inbox.FirstOrDefault(f => f.Id == PushId && f.DXID == DXID)
                : basedb.HighBonus_inbox.FirstOrDefault(f => f.Id == PushId);
            if (inbox == null)
                return null;
            HighPointModel model = JsonConvert.DeserializeObject<HighPointModel>(JsonConvert.SerializeObject(inbox));
            model.VALIDDT = inbox.VALIDDT?.ToString("yyyy/MM/dd") ?? "";
            return model;
        }
        public HighBonusGiveModel GetHighGiveModel(string PushId, string DXID = null)
        {
            HighBonus_inbox inbox;
            inbox = !string.IsNullOrEmpty(DXID)
                ? basedb.HighBonus_inbox.FirstOrDefault(f => f.Id == PushId && f.DXID == DXID)
                : basedb.HighBonus_inbox.FirstOrDefault(f => f.Id == PushId);
            if (inbox == null)
                return null;
            HighBonusGiveModel model = JsonConvert.DeserializeObject<HighBonusGiveModel>(JsonConvert.SerializeObject(inbox));
            model.VALIDDT = inbox.VALIDDT?.ToString("yyyy/MM/dd") ?? "";
            return model;
        }
    }
}