﻿using HighBonus.Entity;
using HighBonus.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace HighBonus.Service
{
    public class Utils
    {
        private static object _object = new object();
        public static string GetAllIntId()
        {

            TimeSpan timespan = TimeSpan.FromTicks(DateTime.Now.Ticks);
            int processId = new Random(Guid.NewGuid().GetHashCode()).Next();

            //ProcessModelInfo.GetCurrentProcessInfo().ProcessID;
            lock (_object)
            {

                int inc = 0;
                if (HttpRuntime.Cache.Get("Inc") != null)
                {
                    inc = Convert.ToInt32(HttpRuntime.Cache.Get("Inc"));
                    inc += 1;
                    HttpRuntime.Cache.Insert("Inc", inc);
                }
                else
                {
                    HttpRuntime.Cache.Insert("Inc", 1);
                }
                string str_result = string.Format("{0}{1}{2}",
                        String.Format("{0:x}", timespan.Seconds),
                        String.Format("{0:x}", processId),
                        String.Format("{0:x}", inc)
                    );

                return str_result;
            }

        }
        /// <summary>  
        /// AES加密  
        /// </summary>  
        /// <param name="SourceStr">要加密的字串</param>  
        /// <param name="key">密鑰串（16位或32位）</param>  
        /// <returns>加密后的字串</returns>  
        public static string Encrypt(string SourceStr, string key, string sIV)
        {
            #region Aes
            Aes aes = new AesCryptoServiceProvider();//RijndaelManaged aes = new RijndaelManaged();
            aes.Key = Encoding.UTF8.GetBytes(key);
            aes.IV = Encoding.UTF8.GetBytes(sIV);
            aes.Mode = CipherMode.CBC;
            aes.Padding = PaddingMode.PKCS7;
            byte[] bytes = Encoding.UTF8.GetBytes(SourceStr);
            try
            {
                byte[] resultBytes = aes.CreateEncryptor().TransformFinalBlock(bytes, 0, bytes.Length);
                string result = Convert.ToBase64String(resultBytes);
                return result;
            }
            catch
            {
                throw;
            }
            #endregion
        }
        /// <summary>  
        /// AES解密  
        /// </summary>  
        /// <param name="data">要解密的字符串</param>  
        /// <param name="sKey">密钥串（16位或32位字符串）</param>  
        /// <returns>解密后的字符串</returns>  
        public static string DecryptAES(string data, string sKey, string sIV)
        {
            try
            {
                Aes aes = new AesCryptoServiceProvider();//RijndaelManaged aes = new RijndaelManaged();
                byte[] bData = Convert.FromBase64String(data); //解密base64;  
                //byte[] bData = HexToByte(data);                  //16进制to byte[];  
                aes.Key = UTF8Encoding.UTF8.GetBytes(sKey);
                aes.IV = UTF8Encoding.UTF8.GetBytes(sIV);
                aes.Mode = CipherMode.CBC;
                aes.Padding = PaddingMode.PKCS7;

                byte[] bResult = aes.CreateDecryptor().TransformFinalBlock(bData, 0, bData.Length);
                return Encoding.UTF8.GetString(bResult);
            }
            catch
            {
                throw;
            }
        }

        #region DESEnCode DES加密
        /// <summary>  
        /// DES加密  
        /// </summary>  
        /// <param name="data">要加密的字符串</param>  
        /// <param name="key">密钥串（8位字符串）</param>  
        /// <returns>加密后的字符串</returns>  
        public static string EncryptDES(string data, string key)
        {
            DES des = new DESCryptoServiceProvider();
            des.Mode = CipherMode.ECB;
            des.Key = Encoding.UTF8.GetBytes(key);
            des.IV = Encoding.UTF8.GetBytes(key);

            byte[] bytes = Encoding.UTF8.GetBytes(data);
            byte[] resultBytes = des.CreateEncryptor().TransformFinalBlock(bytes, 0, bytes.Length);

            return Convert.ToBase64String(resultBytes); //密文以base64返回;(可根据实际需要返回16进制数据;)  
            //return ByteToHex(resultBytes);//十六位  
        }
        #endregion

        #region DESDeCode DES解密
        /// <summary>  
        /// DES解密  
        /// </summary>  
        /// <param name="data">要解密的字符串</param>  
        /// <param name="key">密钥串（8位字符串）</param>  
        /// <returns>解密后的字符串</returns>  
        public static string DecryptDES(string data, string key)
        {
            DES des = new DESCryptoServiceProvider();
            des.Mode = CipherMode.ECB;
            des.Key = Encoding.UTF8.GetBytes(key);
            des.IV = Encoding.UTF8.GetBytes(key);

            byte[] bytes = Convert.FromBase64String(data);
            //byte[] bytes = HexToByte(data);
            byte[] resultBytes = des.CreateDecryptor().TransformFinalBlock(bytes, 0, bytes.Length);

            return Encoding.UTF8.GetString(resultBytes);
        }

        #endregion
        // method to convert hex string into a byte array    
        private static byte[] HexToByte(string data)
        {
            data = data.Replace(" ", "");

            byte[] comBuffer = new byte[data.Length / 2];

            for (int i = 0; i < data.Length; i += 2)
                comBuffer[i / 2] = (byte)Convert.ToByte(data.Substring(i, 2), 16);

            return comBuffer;
        }

        // method to convert a byte array into a hex string    
        private static string ByteToHex(byte[] comByte)
        {
            StringBuilder builder = new StringBuilder(comByte.Length * 3);

            foreach (byte data in comByte)
                builder.Append(Convert.ToString(data, 16).PadLeft(2, '0').PadRight(3, ' '));

            return builder.ToString().ToUpper().Replace(" ", "");
        }




        const string HPUrl = "https://www.toyota.com.tw/collection_app/collection_app.aspx?para=";
        const string aesKey = "App2016125Toyota";
        public string collectionPara(HighPointModel HBModel)
        {
            string result = $"{HBModel.DXID},{HBModel.LICSNO},{HBModel.INVOCID},{DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}";
            result = Encrypt(result, aesKey, aesKey);
            result = HttpContext.Current.Server.UrlEncode(result);
            return result;
        }
        public string HighPointUrl(HighPointModel HBModel)
        {
            string result = $"{HBModel.DXID},{HBModel.LICSNO},{HBModel.INVOCID},{DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}";
            result = Encrypt(result, aesKey, aesKey);
            result = HttpContext.Current.Server.UrlEncode(result);
            result = $"{HPUrl}{result}";
            return result;
        }
        public string HighPointUrl(HighBonus_inbox inbox)
        {
            string result = $"{inbox.DXID},{inbox.LICSNO},{inbox.INVOCID},{DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}";
            result = Encrypt(result, aesKey, aesKey);
            result = HttpContext.Current.Server.UrlEncode(result);
            result = $"{HPUrl}{result}";
            return result;
        }
        public string HighPointUrl(string para)
        {
            string result = $"{HPUrl}{para}";
            return result;
        }
        /// <summary>
        /// 將業主送來的字串不管變數名或值中前後有大量的空白消除
        /// </summary>
        /// <param name="data">json字串</param>
        /// <returns>將變數名或值trim後的json字串</returns>
        public static string TrimJson(string data)
        {
            string result = string.Empty;
            using (StringWriter sw = new StringWriter())
            {
                using (JsonTextWriter writer = new JsonTextWriter(sw))
                {
                    using (StringReader sr = new StringReader(data))
                    {
                        using (JsonTextReader reader = new JsonTextReader(sr))
                        {
                            while (reader.Read())
                            {
                                switch (reader.TokenType)
                                {
                                    case JsonToken.StartArray:
                                        {
                                            writer.WriteStartArray();
                                            break;
                                        }
                                    case JsonToken.StartObject:
                                        {
                                            writer.WriteStartObject();
                                            break;
                                        }
                                    case JsonToken.EndArray:
                                        {
                                            writer.WriteEndArray();
                                            break;
                                        }
                                    case JsonToken.EndObject:
                                        {
                                            writer.WriteEndObject();
                                            break;
                                        }
                                    case JsonToken.PropertyName:
                                        {
                                            writer.WritePropertyName(reader.Value.ToString().Replace(" ", ""));
                                            break;
                                        }
                                    case JsonToken.String:
                                        {
                                            string temp = reader.Value.ToString();
                                            result = temp.Trim();
                                            writer.WriteValue(result);
                                            break;
                                        }
                                    default://目前業主使用的只有字串和整數尚無其它格式，將來視需要再增加
                                        {
                                            if (reader.Value != null)
                                            {
                                                writer.WriteValue(reader.Value);
                                            }
                                            break;
                                        }
                                }
                            }
                        }
                    }
                }
                result = sw.ToString();
            }
            return result;
            #region 原來讀取字串舊的碼
            //using (StringReader sr = new StringReader(highpoint.ADD_LIST_SET.Replace(" ", "")))
            //{
            //    using (JsonTextReader reader = new JsonTextReader(sr))
            //    {
            //        while (reader.Read())
            //        {
            //            if (reader.TokenType == JsonToken.StartObject)
            //            {
            //                // Load each object from the stream and do something with it
            //                JObject obj = JObject.Load(reader);
            //                HighItem newitem = new HighItem();
            //                newitem.Code = obj["商品代碼"].ToString();
            //                newitem.Name = obj["商品名稱"].ToString();
            //                newitem.Point = (int)obj["POINT"];
            //                newitem.Url = obj["商品照片連結"].ToString();
            //                highpoint.HighPointItem.Add(newitem);
            //            }
            //        }
            //    }
            //}
            #endregion
        }

    }
}