﻿using System.Linq;
using HighBonus.Models;
using Newtonsoft.Json;

namespace HighBonus.Service
{
    public class WaitRepairService : BaseService
    {
        public WaitRepairModel Get(string pushId, string dxid = null)
        {
            var inbox = !string.IsNullOrEmpty(dxid)
                ? basedb.WaitRepair_inbox.FirstOrDefault(f => f.Id == pushId && f.DXID == dxid)
                : basedb.WaitRepair_inbox.FirstOrDefault(f => f.Id == pushId);
            if (inbox == null)
                return null;
            WaitRepairModel model = JsonConvert.DeserializeObject<WaitRepairModel>(JsonConvert.SerializeObject(inbox));
            model.LSFXKM = int.Parse(model.LSFXKM).ToString("N0");
            return model;
        }
    }
}