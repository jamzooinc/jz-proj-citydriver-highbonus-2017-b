﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using HighBonus.Entity;
using HighBonus.Models;
using Newtonsoft.Json;

namespace HighBonus.Service
{
    public class RegCareService : BaseService
    {
        public RegCareModel GetRegCareModel(string PushId, string DXID = null)
        {
            var inbox = !string.IsNullOrEmpty(DXID)
                ? basedb.RegCare_inbox.FirstOrDefault(f => f.Id == PushId && f.DXID == DXID)
                : basedb.RegCare_inbox.FirstOrDefault(f => f.Id == PushId);
            if (inbox == null)
                return null;
            RegCareModel model = JsonConvert.DeserializeObject<RegCareModel>(JsonConvert.SerializeObject(inbox));
            return model;
        }
    }
}