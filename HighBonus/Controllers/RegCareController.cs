﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HighBonus.Models;
using HighBonus.Service;

namespace HighBonus.Controllers
{
    public class RegCareController : Controller
    {
        // GET: RegMaint
        public ActionResult Index(string PushId, string DXID)
        {
            var service = new RegCareService();
            RegCareModel model = service.GetRegCareModel(PushId, DXID);
            return View(model);
        }
    }
}