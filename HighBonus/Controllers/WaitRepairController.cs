﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HighBonus.Models;
using HighBonus.Service;

namespace HighBonus.Controllers
{
    public class WaitRepairController : Controller
    {
        // GET: RegMaint
        public ActionResult Index(string pushId, string dxid)
        {
            var service = new WaitRepairService();
            WaitRepairModel model = service.Get(pushId, dxid);
            return View(model);
        }
    }
}