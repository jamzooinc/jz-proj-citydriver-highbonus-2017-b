﻿using HighBonus.Models;
using HighBonus.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HighBonus.Controllers
{
    public class HighBonusGiveController : Controller
    {
        // GET: HighBonus
        //public ActionResult Index()
        //{
        //    HighPointModel model = new HighPointModel();
        //    return View(model);
        //}
        public ActionResult Index(string Id)
        {
            HighBunusInboxService service = new HighBunusInboxService();
            HighBonusGiveModel model = service.GetHighGiveModel(Id);
            return View(model);
        }
    }
}