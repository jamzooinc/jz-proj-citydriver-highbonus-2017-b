﻿using HighBonus.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HighBonus.Areas.App.Service
{
    public class BaseService
    {
        public citydriverEntities basedb;

        public BaseService()
        {
            basedb = new citydriverEntities();
        }

        public static string Domain = System.Web.Configuration.WebConfigurationManager.AppSettings["Domain"];
    }
}