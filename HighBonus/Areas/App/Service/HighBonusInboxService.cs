﻿using APushProvider.Core;
using HighBonus.Areas.App.Models;
using HighBonus.Entity;
using HighBonus.Service;
using Newtonsoft.Json;
using NLog.Internal;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;

namespace HighBonus.Areas.App.Service
{
    public class HighBonusInboxService : BaseService
    {
        public List<HighPointReturnModel> New(string data)
        {
            var returnmodels = new List<HighPointReturnModel>();
            var returnmodel = new HighPointReturnModel();
            List<HighBonus_inbox> inboxs = new List<HighBonus_inbox>();
            data = Utils.TrimJson(data);
            try
            {
                inboxs = JsonConvert.DeserializeObject<List<HighBonus_inbox>>(data);
            }
            catch (Exception ex)
            {
                returnmodel.result_message = $"傳送格式錯誤\n{ex.Message}";
                returnmodel.result_key = "400";
                returnmodel.result = false;
                returnmodels.Add(returnmodel);
                return returnmodels;
            }

            foreach (HighBonus_inbox inbox in inboxs)
            {
                returnmodel = new HighPointReturnModel();
                #region 檢查有無訂閱並產生收件夾資料
                var chkdevice = basedb.regularcare_device.Where(f => f.Dxid == inbox.DXID && f.IsClose == 0).Select(f => f.Dxid).ToList();
                if (chkdevice.Count > 0)
                {
                    #region 產生收件夾資料
                    inbox.Id = Utils.GetAllIntId();
                    //inbox.IsExpireNotify = false;
                    inbox.NotifyType = 4;
                    inbox.CreateDate = DateTime.Now;
                    inbox.PROCDT = DateTime.Now;
                    inbox.Status = 0;
                    inbox.Title = $"TOYOTA車主權益提醒：您的利high金共有{inbox.POINT.Value.ToString("#,###")}點，歡迎詢問兌換";
                    basedb.HighBonus_inbox.Add(inbox);
                    try
                    {
                        basedb.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        returnmodel.result_message = $"建立發送錯誤\n{ex.Message}";
                        returnmodel.result_key = "500";
                        returnmodel.result = false;
                        returnmodels.Add(returnmodel);
                        break;
                    }
                    #endregion
                }
                else
                {
                    returnmodel.result_message = "DXID無訂閱";
                    returnmodel.result_key = "400";
                    returnmodel.result = false;
                    returnmodels.Add(returnmodel);
                    break;
                }
                #endregion

                #region 讀取收件夾，寫收件夾文字檔並發推播
                string path = System.Web.Configuration.WebConfigurationManager.AppSettings["p12Path"];
                string p12Psd = System.Web.Configuration.WebConfigurationManager.AppSettings["p12Psd"];
                string apikey_phone = System.Web.Configuration.WebConfigurationManager.AppSettings["apikey_phone"];

                var pushdata = from p in basedb.HighBonus_inbox
                               join d in basedb.regularcare_device on p.DXID equals d.Dxid
                               where (p.Status == 0 || p.Status == 4) && p.Id == inbox.Id
                               orderby p.CreateDate descending
                               select new
                               {
                                   p = p,
                                   d = d,
                               };
                if (pushdata != null)
                {
                    #region 寫收件夾文字檔
                    if (!returnmodel.result) break;
                    DateTime dt = DateTime.Now;
                    string inboxpath = System.Web.Configuration.WebConfigurationManager.AppSettings["inboxpath"];
                    try
                    {
                        var query = basedb.regularcare_inbox.
                            Where(p => p.Status != 3 && (DbFunctions.TruncateTime(dt) >= DbFunctions.TruncateTime(p.CreateDate)) && p.DXID == inbox.DXID).
                            Select(p => new
                            {
                                p.Id,
                                p.DXID,
                                p.PROCDT,
                                p.Title,
                                p.Status,
                                p.CreateDate,
                                p.ISFROIL
                            });

                        var queryContinueSafe = basedb.regularcare_inbox_Safe.
                            Where(p => p.Status != 3 && (DbFunctions.TruncateTime(dt) >= DbFunctions.TruncateTime(p.CreateDate)) && p.DXID == inbox.DXID).
                            Select(p => new
                            {
                                p.Id,
                                p.DXID,
                                p.CRTDT,
                                p.TITLE,
                                p.Status,
                                p.Type,
                                p.CreateDate
                            });
                        var queryHighBonus = basedb.HighBonus_inbox
                            .Where(p => p.Status != 3 && (p.Status != 0 || p.Id == inbox.Id )
                            && (DbFunctions.TruncateTime(dt) >= DbFunctions.TruncateTime(p.CreateDate)) && p.DXID == inbox.DXID).
                                        Select(p => new
                                        {
                                            p.Id,
                                            p.DXID,
                                            p.PROCDT,
                                            p.Title,
                                            p.Status,
                                            p.CreateDate,
                                            p.NotifyType
                                        });
                        List<InBoxModel> InBox = new List<InBoxModel>();
                        try
                        {
                            InBox.AddRange(query.Where(p => p.DXID == inbox.DXID).Select(o_entity =>
                                            new InBoxModel()
                                            {
                                                PushId = o_entity.Id,
                                                DateTimes = o_entity.CreateDate,
                                                Title = o_entity.Title,
                                                Status = o_entity.Status.Value == 0 ? 1 : o_entity.Status.Value,
                                                Type = (o_entity.ISFROIL == "Y") ? 3 : 0, 
                                            }
                                            ).ToList());
                        }
                        catch
                        { }
                        try
                        {
                            InBox.AddRange(queryContinueSafe.Where(p => p.DXID == inbox.DXID).Select(o_entity =>
                                            new InBoxModel()
                                            {
                                                PushId = o_entity.Id,
                                                DateTimes = o_entity.CreateDate,
                                                Title = o_entity.TITLE,
                                                Status = o_entity.Status.Value == 0 ? 1 : o_entity.Status.Value,
                                                Type = o_entity.Type == 0 ? 1 : 2
                                            }
                                            ).ToList());
                        }
                        catch
                        { }
                        try
                        {
                            InBox.AddRange(queryHighBonus.Where(p => p.DXID == inbox.DXID).Select(o_entity =>
                                            new InBoxModel
                                            {
                                                PushId = o_entity.Id,
                                                DateTimes = o_entity.CreateDate,
                                                Title = o_entity.Title,
                                                Status = o_entity.Status.Value == 0 ? 1 : o_entity.Status.Value,
                                                Type = o_entity.NotifyType
                                            }
                                            ).ToList());
                        }
                        catch
                        { }
                        //0：定保；1：保險函；2：感謝函 ; 3:機油券; 4:利high金; 5:利high金到期提醒

                        InBoxListModel Page = new InBoxListModel();

                        //0未發送 1已發送 2已讀 3已刪除 4發送失敗 5推播關閉
                        Page.TotalUnreadCount = InBox.Where(p => p.Status != 2).Count();
                        Page.DXID = inbox.DXID;
                        Page.Data = InBox.OrderByDescending(p => p.DateTimes).ToList();

                        foreach (var d in Page.Data)
                        {
                            d.Date = (int)((d.DateTimes.AddHours(-8).Subtract(new DateTime(1970, 1, 1))).TotalSeconds);
                        }

                        //寫入txt
                        string jsonString = JsonConvert.SerializeObject(Page);

                        // 建立檔案串流
                        if (!Directory.Exists(inboxpath))
                        {
                            //當目錄不存在則新建資料夾
                            Directory.CreateDirectory(inboxpath);
                        }
                        StreamWriter sw = new StreamWriter(inboxpath + inbox.DXID + ".txt", false);
                        sw.WriteLine(jsonString);       // 寫入文字
                        sw.Close();                     // 關閉串流
                    }
                    catch (Exception e2)
                    {
                        returnmodel.result_message = $"寫入TXT發生錯誤：{e2.Message} 檔名路徑：{inboxpath}{inbox.DXID}.txt /n";
                        returnmodel.result_key = "400";
                        returnmodel.result = false;
                        returnmodels.Add(returnmodel);
                        break;
                    }

                    #endregion
                    #region 利high金推播
                    var appleCert = File.ReadAllBytes(path);

                    try
                    {
                        GcmSender GcmSender_PHONE = new GcmSender(apikey_phone, true);

                        //string errormsg = string.Empty;
                        int i = 0;
                        foreach (var item in pushdata.ToList())
                        {
                            i++;
                            bool IsPush = false;
                            int Status = 1;
                            //errormsg = string.Empty; //每次都要清空, 才能辨別不同裝置的訊息
                            string _dxid = item.p.DXID;

                            //1.組成網頁推播的URL
                            //url = $"http://hotaiweb01.cloudapp.net/HighBonus/HighBonus/Index?Id={item.p.Id} + "&DXID=" + _dxid;

                            //2.尋找相對應的token & OS
                            var device = item.d;

                            if (device.IsClose == 0)
                            {
                                // 發推播通知 - 只針對車機板 Alert=標題 , ActionLocKey = 按鈕文字
                                if (device.OS.ToLower() == "ios")
                                {
                                    AppleSender ApnsSender = new AppleSender(appleCert, p12Psd, ReturnLog: true);
                                    ApnsSender.SetCustomItems("{\"LitePushPid\":\"" + item.p.Id + "\",\"Type\":\"4\"}");
                                    ApnsSender.SetText(item.p.Title);
                                    ApnsSender.SetButtonText("開啟");
                                    ApnsSender.SetBadge(1);
                                    List<string> AnLt = new List<string>();
                                    AnLt.Add(device.Token);
                                    ApnsLogList failLog = ApnsSender.Send(AnLt);

                                    if (getMsgFromAPNS(failLog))
                                    {
                                        IsPush = true;
                                        returnmodel.result_message += "推播成功";
                                        returnmodel.result_key = "200";
                                        returnmodel.result = true;
                                        returnmodels.Add(returnmodel);
                                    }
                                    else
                                    {
                                        Status = 4;
                                        returnmodel.result_message += $"此DXID={_dxid }推播失敗{failLog.FailItems.LastOrDefault().FailMessage}";
                                        returnmodel.result_key = "400";
                                        returnmodel.result = false;
                                        returnmodels.Add(returnmodel);
                                    }
                                }
                                else
                                {
                                    GcmSender_PHONE = new GcmSender(apikey_phone, true);
                                    GcmSender_PHONE.SetText(item.p.Title);
                                    GcmSender_PHONE.SetButtonText("開啟");
                                    GcmSender_PHONE.SetCustomItems("{\"LitePushPid\":\"" + item.p.Id + "\",\"Type\":\"4\"}");
                                    GcmLogList failLog = GcmSender_PHONE.Send(new List<string>() { device.Token });
                                    //如果FailItems找不到該Token的錯誤訊息，即為發送OK
                                    if (getMsgFromGCM(failLog))
                                    {
                                        IsPush = true;
                                        returnmodel.result_message += "推播成功";
                                        returnmodel.result_key = "200";
                                        returnmodel.result = true;
                                        returnmodels.Add(returnmodel);
                                    }
                                    else
                                    {
                                        Status = 4;
                                        returnmodel.result_message += "此DXID=" + _dxid + "推播失敗" + failLog.FailItems.LastOrDefault().FailMessage;
                                        returnmodel.result_key = "400";
                                        returnmodel.result = false;
                                        returnmodels.Add(returnmodel);
                                    }

                                }
                            }
                            else
                            {
                                Status = 5;
                                returnmodel.result_message += $"This DXID={_dxid}is Closed (only Android have this function)";
                                returnmodel.result_key = "400";
                                returnmodel.result = false;
                                returnmodels.Add(returnmodel);
                            }
                            // 紀錄PUSHLOG
                            HighBonus_pushlog log = new HighBonus_pushlog();
                            log.CreateDate = DateTime.Now;
                            log.DXID = inbox.DXID;
                            log.PushId = inbox.Id;
                            Type t = returnmodel.GetType();
                            PropertyInfo[] props = t.GetProperties();
                            foreach (var prop in props)
                                if (prop.GetIndexParameters().Length == 0)
                                    log.Result += prop.GetValue(returnmodel) + " ";

                            // 以及修改收件夾狀態
                            var r = (from p in basedb.HighBonus_inbox
                                     where p.Id == inbox.Id
                                     select p).FirstOrDefault();

                            r.Status = Status; //修改狀態成已發送
                            r.CreateDate = DateTime.Now;
                            log.Status = Status;

                            basedb.HighBonus_pushlog.Add(log);
                            basedb.SaveChanges();
                        }
                    }
                    catch (Exception ex)
                    {
                        returnmodel.result_message += ex.Message;
                        returnmodel.result_key = "500";
                        returnmodel.result = false;
                        returnmodels.Add(returnmodel);
                        try
                        {
                            StreamReader sr = new StreamReader(inboxpath + inbox.DXID + ".txt",  Encoding.UTF8);

                            string end = sr.ReadToEnd();
                            sr.Close();// 關閉串流

                            InBoxListModel list = JsonConvert.DeserializeObject<InBoxListModel>(end);

                            InBoxModel TXTModel = list.Data.FirstOrDefault(p => p.PushId == inbox.Id);
                            if (TXTModel != null)
                            {
                                if (TXTModel.Status != 2)
                                {
                                    //如果是刪除未讀信，則未讀數減一
                                    if (list.TotalUnreadCount > 0)
                                    {
                                        list.TotalUnreadCount = list.TotalUnreadCount - 1;
                                    }
                                }
                                list.Data.Remove(TXTModel);
                            }
                            end = JsonConvert.SerializeObject(list);
                            StreamWriter sw = new StreamWriter(inboxpath + inbox.DXID + ".txt", false);
                            sw.WriteLine(end);      // 寫入文字
                            sw.Close();
                        }
                        catch (Exception EX)
                        {
                        }
                    }
                    #endregion
                }
                else
                {
                    returnmodel.result_message = "推播失敗";
                    returnmodel.result_key = "400";
                    returnmodel.result = false;
                    returnmodels.Add(returnmodel);
                }
                #endregion
            }

            return returnmodels;
        }
        public HighPointReturnModel HaseRead(string Id)
        {
            var result = new HighPointReturnModel();
            var box = basedb.HighBonus_inbox.Join(
                      basedb.HighBonus_pushlog,
                      p => p.Id,
                      log => log.PushId,
                      (p, log) =>
                         new
                         {
                             p = p,
                             log = log
                         }
                   ).FirstOrDefault();
            box.p.Status = 2;
            box.log.Status = 2;
            try
            {
                basedb.SaveChanges();
                result.result_key = "200";
                result.result = true;
                result.result_message = "";
            }
            catch (Exception ex)
            {
                result.result_key = "500";
                result.result = false;
                result.result_message = "變更已讀失敗";
            }
            return result;
        }
        public HighPointReturnModel Delete(string Id)
        {
            var result = new HighPointReturnModel();
            var box = basedb.HighBonus_inbox.Join(
                      basedb.HighBonus_pushlog,
                      p => p.Id,
                      log => log.PushId,
                      (p, log) =>
                         new
                         {
                             p = p,
                             log = log
                         }
                   ).FirstOrDefault();
            box.p.Status = 3;
            box.log.Status = 3;
            try
            {
                basedb.SaveChanges();
                result.result_key = "200";
                result.result = true;
                result.result_message = "";
            }
            catch (Exception ex)
            {
                result.result_key = "500";
                result.result = false;
                result.result_message = "變更刪除失敗";
            }
            return result;
        }
        public static bool getMsgFromGCM(GcmLogList list)
        {
            if (list.OkItems.Count() != 0)
            {
                //if (list.OkItems.LastOrDefault().FailMessage.ToLower().Equals("ok"))
                //{
                //    return true;
                //}
                return true;
            }
            else if (list.FailItems.Count() != 0)
            {
                if (!string.IsNullOrEmpty(list.FailItems.FirstOrDefault().FailMessage))
                {
                    if (list.FailItems.FirstOrDefault().FailMessage.ToLower().Equals("ok"))
                        return true;
                }
            }
            return false;
        }

        public static bool getMsgFromAPNS(ApnsLogList list)
        {
            if (list.OkItems.Count() != 0)
            {
                return true;
            }
            else if (list.FailItems.Count() != 0)
            {
                if (!string.IsNullOrEmpty(list.FailItems.FirstOrDefault().FailMessage))
                {
                    if (list.FailItems.FirstOrDefault().FailMessage.ToLower().Equals("ok"))
                        return true;
                }
            }
            return false;
        }

        public string GetCollectionUrl(string Id)
        {
            HighBonus_inbox inbox = basedb.HighBonus_inbox.Where(p => p.Id == Id).FirstOrDefault();
            Utils survice = new Utils();
            string result = survice.HighPointUrl(inbox);
            return result;
        }

    }
}