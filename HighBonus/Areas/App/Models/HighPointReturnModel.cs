﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HighBonus.Areas.App.Models
{
        public class HighPointReturnModel
        {
            //推播用基本欄位
            public string result_key { get; set; }
            public bool result { get; set; }
            public string result_message { get; set; }
            //public string result_url { get; set; }
            public HighPointReturnModel()
            {
                result_key = "";
                result = true;
                result_message = "";
                //result_url = " http://www.toyota.com.tw/TOYOTA_Loyalty_Program/highbonus.html";
            }
        }
}