﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HighBonus.Areas.App.Models
{
    public class Inbox
    {
    }
    public class InBoxModel
    {
        //類別0：定保；1：保險函；2：感謝函; 3:機油券; 4:利high金
        public int Type { get; set; }
        //訊息編號
        public string PushId { get; set; }

        //推播標題
        public string Title { get; set; }

        //訊息狀態 0未發送 1已發送=未讀 2已讀 3已刪除
        public int Status { get; set; }

        //訊息發佈日期，TimeStamp，取到「毫秒」
        public DateTime DateTimes { get; set; }

        public double Date { get; set; }

    }
    public class InBoxListModel
    {
        public int TotalUnreadCount { get; set; }
        public string DXID { get; set; }
        public List<InBoxModel> Data { get; set; }
        public InBoxListModel()
        {
            Data = new List<InBoxModel>();
        }
    }


}