﻿using HighBonus.Areas.App.Service;
using HighBonus.Models;
using HighBonus.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HighBonus.Areas.App.Models;

namespace HighBonus.Areas.App.Controllers
{
    public class HighPointController : Controller
    {
        // GET: App/HighPoint
        public ActionResult Index()
        {
            HighPointModel model = new HighPointModel();
            model.DXID = "926e0366-4084-4e9e-8594-d83080f5340c";
            model.LICSNO = "RBD-5256";
            model.INVOCID = "T123424087";
            Utils survice = new Utils();
            model.Url = survice.HighPointUrl(model);
            return View(model);
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult HighPoint_New(string data)
        {
            data = !string.IsNullOrEmpty(data) ? data : @"[{
""DXID"":""32dbcb2f-212d-47ef-89c5-2af162edc4f9"", // DXID
""LICSNO"":"" **T-6131"", // 車號
""POINT"": 1046, // 該車號擁有點數
""PARTNO1"":""HHC0214400BL"", //精品代碼1
""PARTNM1"":""TOYOTA食物保溫"", //精品名稱1
""POINT1"": 972, //需要點數1
""IMG1"":"" //hotaicdn.azureedge.net/toyotaweb/20141226154031B9956776.jpg"", //精品照片1
""PARTNO2"":""DLC1116400BL"", //精品代碼2
""PARTNM2"":""紳士風尚名片夾"", //精品名稱2
""POINT2"": 900, //需要點數2
""IMG2"":"" //hotaicdn.azureedge.net/toyotaweb/GOODS_20170116182401EO6IH8O1.jpg"", //精品照片2
""PARTNO3"":""DCF0116200BK"", //精品代碼3
""PARTNM3"":""不倒馬克杯 (黑色)"", //精品名稱3
""POINT3"": 882, //需要點數3
""IMG3"":"" //hotaicdn.azureedge.net/toyotaweb/201606141114352D3MBFF3.jpg"" //精品照片3 
}]";

            HighBonusInboxService service = new HighBonusInboxService();
            var ReruenModel = service.New(data);
            return Json(ReruenModel, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult HighPointRead(string Id)
        {
            HighBonusInboxService service = new HighBonusInboxService();
            HighPointReturnModel ReruenModel = service.HaseRead(Id);
            return Json(ReruenModel, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult HighPointDelete(string Id)
        {
            HighBonusInboxService service = new HighBonusInboxService();
            HighPointReturnModel ReruenModel = service.Delete(Id);
            return Json(ReruenModel, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        [ValidateInput(false)]
        public JsonResult ColloctionUrl(string Id)
        {
            HighBonusInboxService service = new HighBonusInboxService();
            string result = service.GetCollectionUrl(Id);
            return Json(result);
        }
    }
}